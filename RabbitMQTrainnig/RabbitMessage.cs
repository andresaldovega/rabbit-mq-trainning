﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQTrainnig
{
    public class RabbitMessage
    {
        public string From { get; set; }
        public string Message { get; set; }
    }
}
